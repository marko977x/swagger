﻿using DataService.Models;
using Microsoft.EntityFrameworkCore;

namespace DataService.Data
{
    public class SensorContext : DbContext
    {
        public DbSet<SensorData> SensorsData { get; set; }

        public SensorContext(DbContextOptions<SensorContext> contextOptions)
            : base(contextOptions) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SensorData>().ToTable("SensorData");
        }
    }
}
