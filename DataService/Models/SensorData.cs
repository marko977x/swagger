﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataService.Models
{
    public class SensorData
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        /// <summary>
        /// Sensor type from which data is retrieved
        /// </summary>
        /// <example>temperature</example>
        [Required]
        public string SensorType { get; set; }

        /// <summary>
        /// Time when data is recorded
        /// </summary>
        /// <example>01/01/2020 15:45:01</example>
        public string RecordTime { get; set; }

        /// <summary>
        /// Retrieved data from sensor
        /// </summary>
        /// <example>20</example>
        [Required]
        public double Value { get; set; }
    }
}
