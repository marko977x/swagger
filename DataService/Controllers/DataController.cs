﻿using DataService.Data;
using DataService.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataService.Controllers
{
    [Route("api/[controller]/[action]")]
    [Produces("application/json")]
    [ApiController]
    public class DataController : ControllerBase
    {
        private readonly SensorContext _context;

        public DataController(SensorContext context)
        {
            _context = context;
        }

        [HttpGet]
        [SwaggerOperation(Summary = "Get all sensor data")]
        [SwaggerResponse(200, "Data successfully retrieved", typeof(List<SensorData>))]
        [ApiExplorerSettings(GroupName = "v1")]
        public async Task<IActionResult> GetAllData()
        {
            return Ok(await _context.SensorsData.ToListAsync());
        }

        [HttpPost]
        [SwaggerOperation(Summary = "Add data retrieved from sensor")]
        [SwaggerResponse(200, "New sensor value successfully added")]
        [SwaggerResponse(400, "Invalid data")]
        [ApiExplorerSettings(GroupName = "v1")]
        public async Task<IActionResult> AddData([FromBody, Required]SensorData sensorData)
        {
            if (sensorData == null) return BadRequest();
            _context.SensorsData.Add(sensorData);
            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpGet]
        [SwaggerOperation(Summary = "Get sensor data of specific type")]
        [SwaggerResponse(200, "Data successfully retrieved", typeof(List<SensorData>))]
        [SwaggerResponse(400, "Invalid sensor type")]
        [ApiExplorerSettings(GroupName = "v1")]
        public async Task<IActionResult> GetDataFromSensor([Required]string sensorType)
        {
            IQueryable<SensorData> sensorsData =
                from data in _context.SensorsData
                where data.SensorType == sensorType
                select data as SensorData;

            return Ok(await sensorsData.ToListAsync());
        }

        [HttpGet]
        [SwaggerOperation(Summary = "Get sensor data of specified type threshold")]
        [SwaggerResponse(200, "Data successfully retrieved", typeof(List<SensorData>))]
        [SwaggerResponse(400, "Invalid sensor type or threshold value format")]
        [ApiExplorerSettings(GroupName = "v2")]
        public async Task<IActionResult> GetDataWithThresholdFromSensor(
            [Required]string sensorType, [Required]double threshold)
        {
            IQueryable<SensorData> sensorsData =
                from data in _context.SensorsData
                where data.SensorType == sensorType && data.Value >= threshold
                select data as SensorData;

            return Ok(await sensorsData.ToListAsync());
        }

        [HttpGet]
        [SwaggerOperation(Summary = "Example of some obsolete action")]
        [ApiExplorerSettings(GroupName = "v1")]
        [Obsolete]
        public IActionResult SomeObsoleteAction()
        {
            return BadRequest();
        }
    }
}
