﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace DeviceService.Services
{
    [SwaggerSchema("Class that abstracts sensor functioning")]
    public class Sensor
    {
        public bool IsMeasuring { get; set; }
        public string Type { get; set; }
        public bool IsTreshold { get; set; }
        public double TresholdValue { get; set; }
        public double Timeout { get; set; }
        public double Value { get; set; }

        private readonly Timer _timer;
        private readonly StreamReader _streamReader;

        public Sensor(string type, string path, double treshold)
        {
            IsMeasuring = true;
            IsTreshold = false;
            Timeout = 5000;
            TresholdValue = treshold;
            Type = type;

            _timer = new Timer(Timeout);
            _timer.Elapsed += Timer_Elapsed;
            _timer.Start();
            _streamReader = new StreamReader(path);
        }

        public void SetTimeout(double interval)
        {
            _timer.Stop();
            Timeout = interval;
            _timer.Interval = interval;
            _timer.Start();
        }

        private async void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ReadValue();
            await SendValueAsync();
        }

        public void StopSensor()
        {
            IsMeasuring = false;
            _timer.Stop();
        }
        public void StartSensor()
        {
            IsMeasuring = true;
            _timer.Start();
        }
        private async Task SendValueAsync()
        {
            if (IsTreshold)
            {
                if (Value > TresholdValue)
                    await PostRequest("http://swagger_dataservice_1/api/Data/AddData");
            }
            else
            {
                await PostRequest("http://swagger_dataservice_1/api/Data/AddData");
            }
        }

        private async Task PostRequest(string uri)
        {
            HttpClient httpClient = new HttpClient();
            string data = System.Text.Json.JsonSerializer.Serialize(new
            {
                RecordTime = DateTime.Now.ToShortTimeString(),
                SensorType = Type,
                Value
            });

            Console.WriteLine(data);

            try
            {
                await httpClient.PostAsync(uri, new StringContent(
                    System.Text.Json.JsonSerializer.Serialize(new
                    {
                        RecordTime = DateTime.Now.ToShortTimeString(),
                        SensorType = Type,
                        Value
                    }
                ), Encoding.UTF8, "application/json"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public void ReadValue()
        {
            try
            {
                string line;
                if (!_streamReader.EndOfStream)
                    line = _streamReader.ReadLine();
                else
                {
                    _streamReader.DiscardBufferedData();
                    _streamReader.BaseStream.Seek(0, SeekOrigin.Begin);
                    line = _streamReader.ReadLine();
                }
                Value = double.Parse(line, CultureInfo.InvariantCulture);
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
        }
    }
}
