﻿using Microsoft.Extensions.DependencyInjection;

namespace DeviceService.Services
{
    public static class ServicesCollectionExtension
    {
        public static void AddSensorsServices(this IServiceCollection services)
        {
            services.AddSingleton<SensorsService>(new SensorsService());
        }
    }
}
