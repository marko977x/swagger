﻿using System.Collections.Generic;

namespace DeviceService.Services
{
    public class SensorsService
    {
        public List<Sensor> SensorsList { get; set; }

        public SensorsService()
        {
            this.SensorsList = new List<Sensor>
            {
                new Sensor("temperature", "./temperature.txt", 20.0),
                new Sensor("humidity", "./humidity.txt", 50.0),
                new Sensor("pressure", "./pressure.txt", 1010.0)
            };
        }

        public void AddSensor(Sensor newSensor)
        {
            this.SensorsList.Add(newSensor);
        }

    }
}
