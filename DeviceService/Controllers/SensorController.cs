﻿using DeviceService.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json;

namespace DeviceService.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [SwaggerTag("Sensor Metadata")]
    public class SensorController : ControllerBase
    {

        private readonly SensorsService _sensorsService;
        public SensorController(SensorsService sensorService)
        {
            _sensorsService = sensorService;
        }


        [HttpPost]
        [SwaggerOperation(
            Summary = "Turn on/off specified sensor",
            Description = " Provide sensor type",
            OperationId = "GetTreshold",
            Tags = new[] { "Basic" }
        )]
        [SwaggerResponse(200, "Successful Post Request, sensor turned on/off")]
        [SwaggerResponse(400, "Bad Request, type of sensor doesn't exist. Check provided parameters")]
        public IActionResult TurnOnOffSensor(
            [Required]bool on, [Required]string type)
        {
            foreach (Sensor sensor in _sensorsService.SensorsList)
            {
                if (type == sensor.Type)
                {
                    if (on)
                    {
                        sensor.StartSensor();
                        return Ok($"Sensor {type} turned on");
                    }
                    else
                    {
                        sensor.StopSensor();
                        return Ok($"Sensor {type} turned off");
                    }
                }
            }
            return BadRequest("Type of sensor doesn't exist");
        }


        [HttpGet]
        [SwaggerOperation(
            Summary = "Get specified sensor treshold",
            Description = "Check if sensor is set to trshold based measuring and treshold value. Provide sensor type",
            OperationId = "GetTreshold",
            Tags = new[] { "Treshold" }
        )]
        [SwaggerResponse(200, "Successful Get Request, treshold info received")]
        [SwaggerResponse(400, "Bad Request, type of sensor doesn't exist. Check provided parameters")]
        public IActionResult GetThreshold([Required]string type)
        {
            foreach (var sensor in _sensorsService.SensorsList)
            {
                if (type == sensor.Type)
                {
                    var options = new JsonSerializerOptions
                    {
                        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                        WriteIndented = true
                    };
                    string tresholdInfo = JsonSerializer.Serialize(new { isTreshold = sensor.IsTreshold, value = sensor.TresholdValue }, options);
                    return Ok(tresholdInfo);
                }
            }
            return BadRequest("Type of sensor doesn't exist");
        }


        [HttpPost]
        [SwaggerOperation(
            Summary = "Set specified sensor treshold",
            Description = "Set specified sensor to work in treshold based mode and provide its treshold value",
            OperationId = "SetThreshold",
            Tags = new[] { "Treshold" }
        )]
        [SwaggerResponse(200, "Successful Get Request, treshold set")]
        [SwaggerResponse(400, "Bad Request, type of sensor doesn't exist. Check provided parameters")]
        public virtual IActionResult SetThreshold(
            [Required]string type, double? value)
        {
            if (value == null) return BadRequest("Provide treshold value");

            foreach (var sensor in _sensorsService.SensorsList)
            {
                if (type == sensor.Type)
                {
                    sensor.IsTreshold = true;
                    if (value != null)
                    {
                        sensor.TresholdValue = (double)value;
                        return Ok($"Treshold based measuring started for {type} sensor. New Treshold value set");
                    }
                    else
                    {
                        return Ok($"Treshold based measuring started for {type} sensor. Default Treshold value used");
                    }
                }
            }
            return BadRequest("Type of sensor doesn't exist");
        }


        [HttpGet]
        [SwaggerOperation(
            Summary = "Get specified sensor timeout",
            Description = "Check if sensor is set to timeout based measuring and its timeout value. Provide sensor type",
            OperationId = "SetTimeout",
            Tags = new[] { "Timeout" }
        )]
        [SwaggerResponse(200, "Successful Get Request, timeout info received")]
        [SwaggerResponse(400, "Bad Request, type of sensor doesn't exist. Check provided parameters")]
        public virtual IActionResult GetTimeout([Required]string type)
        {
            foreach (var sensor in _sensorsService.SensorsList)
            {
                if (type == sensor.Type)
                {
                    var options = new JsonSerializerOptions
                    {
                        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                        WriteIndented = true
                    };

                    string timeoutInfo = JsonSerializer.Serialize(new
                    {
                        isTimeout = !sensor.IsTreshold,
                        value = sensor.Timeout
                    }, options);

                    return Ok(timeoutInfo);
                }
            }
            return BadRequest("Type of sensor doesn't exist");
        }


        [HttpPost]
        [SwaggerOperation(
            Summary = "Set specified sensor timeout",
            Description = "Set specified sensor to work in timeout based measuring, and provide its timeout value in miliseconds",
            OperationId = "SetTimeout",
            Tags = new[] { "Timeout" }
        )]
        [SwaggerResponse(200, "Successful POST Request, timeout set", Type = typeof(string))]
        [SwaggerResponse(400, "Bad Request, type of sensor doesn't exist. Check provided parameters!")]
        public virtual IActionResult SetTimeout(
            [Required]string type, double? value)
        {
            foreach (var sensor in this._sensorsService.SensorsList)
            {
                if (type == sensor.Type)
                {
                    sensor.IsTreshold = false;
                    if (value != null)
                    {
                        sensor.SetTimeout((double)value);
                        return Ok($"Timeout based measuring started for {type} sensor. New Timeout value set");
                    }
                    else
                    {
                        return Ok($"Timeout based measuring started for {type} sensor. Default Timeout value used");
                    }
                }
            }
            return BadRequest("Type of sensor doesn't exist");
        }


        [HttpGet]
        [SwaggerOperation(
            Summary = "Get sensors <b>matadata</b>",
            Description = "You can provide specific sensor which metadata you want to check or see all sensors metadata with no parameter provided",
            OperationId = "GetTypesOfSensors",
            Tags = new[] { "Basic" }
        )]
        [SwaggerResponse(200, "Successful GET Request for sensor metadata", Type = typeof(List<Sensor>))]
        [SwaggerResponse(400, "Bad Request, type of sensor doesn't exist. Check provided parameters!")]
        public virtual IActionResult GetSensorsMeatadata(string type)
        {
            if (type == null)
                return Ok(_sensorsService.SensorsList);
            List<Sensor> sendList = new List<Sensor>();
            foreach (var sensor in _sensorsService.SensorsList)
            {
                if (type == sensor.Type)
                    sendList.Add(sensor);
            }
            if (sendList.Count == 0)
                return BadRequest("Type of sensor doesn't exist");
            return Ok(sendList);
        }
    }
}
